AFRAME.registerComponent('stackable', {
    dependencies: ['raycaster'],
    schema: {
        on: {type: 'string'}
    },

    init: function () {
        var data = this.data;
        var el = this.el;

        el.addEventListener(data.on, function (evt) {
            var intersection = evt.detail.intersection;
            console.log(evt.detail.intersection);
            if (!intersection) { return; }
            var mat = intersection.object.matrixWorld;
            // remove parallel movement from the matrix
            mat.setPosition(new THREE.Vector3(0, 0, 0));

            // change local normal into global normal
            var global_normal = intersection.face.normal.clone().applyMatrix4(mat).normalize();

            // look at target coordinate = intersection coordinate + global normal vector
            var lookAtTarget = new THREE.Vector3().addVectors(intersection.point, global_normal);

            var point = intersection.point;
            var markerEl = document.querySelector('#marker');
            markerEl.setAttribute('position', point);
            markerEl.setAttribute('visible', true);
            markerEl.object3D.lookAt(lookAtTarget);
            var menuEl = document.querySelector('#menu');
            menuEl.setAttribute('position', new THREE.Vector3(point.x, point.y+1, point.z));
            menuEl.setAttribute('visible', true);

            if (el.getAttribute('id') == null ) {
                var markedEl = document.querySelector('#marked');
                if (markedEl != null) {
                    markedEl.setAttribute('material', "shader: standard; color: #999999");
                    markedEl.removeAttribute('id');
                }
                el.setAttribute('material', "shader: standard; color: #355C7D");
                el.setAttribute('id', "marked");
            }


            var axisEl = document.querySelector('#axis-system');
            var copy = axisEl.cloneNode(true);
            el.parentNode.appendChild(copy);
            axisEl.parentNode.removeChild(axisEl);

            axisEl = document.querySelector('#axis-system');
            axisEl.setAttribute('visible', el.getAttribute('id')!=="plane");


            // reset menu
            var menu_main = document.querySelector('#menu-main');
            var menu_geometry = document.querySelector('#menu-geometry');
            var menu_property = document.querySelector('#menu-property');
            var menu_resize = document.querySelector('#menu-resize');
            var menu_rotate = document.querySelector('#menu-rotate');
            var menu_reposition = document.querySelector('#menu-reposition');

            menu_geometry.setAttribute('position', "0 0 -1");
            menu_geometry.setAttribute('visible', "false");
            menu_property.setAttribute('position', "0 0 -1");
            menu_property.setAttribute('visible', "false");
            menu_resize.setAttribute('position', "0 0 -1");
            menu_resize.setAttribute('visible', "false");
            menu_rotate.setAttribute('position', "0 0 -1");
            menu_rotate.setAttribute('visible', "false");
            menu_reposition.setAttribute('position', "0 0 -1");
            menu_reposition.setAttribute('visible', "false");
            menu_main.setAttribute('position', "0 0 0");
            menu_main.setAttribute('visible', "true");
        });
    }
});

AFRAME.registerComponent('selectable', {
    schema: {
        on: {type: 'string'},
        src: {type: 'string'}
    },

    init: function () {
        var data = this.data;
        var el = this.el;

        el.addEventListener(data.on, function (evt) {
            var markerEl = document.querySelector('#marker');
            var menuEl = document.querySelector('#menu');
            var sceneEl = document.querySelector('a-scene');

            if (markerEl.getAttribute('visible') === false) {return;}
            markerEl.setAttribute('visible', "false");
            menuEl.setAttribute('visible', "false");

            var matrix = new THREE.Matrix4();
            matrix.extractRotation( markerEl.object3D.matrix );

            var direction = new THREE.Vector3( 0, 0, 1 );
            direction = matrix.multiplyVector3( direction );

            var scale=0.2;

            var entityEl = document.querySelector("#" + data.src + "-prototype");

            // var newWrapperEl = document.createElement('entity');
            // newWrapperEl.setAttribute('class', "geometry");
            // newWrapperEl.setAttribute('id', "prototype");
            // var newGeometryEl = document.createElement('entity');
            // console.log(data.src);
            // switch (data.src) {
            //     case "box":
            //         newGeometryEl.setAttribute('geometry', "primitive: box; depth: 0.4; height: 0.4; width: 0.4");
            //         break;
            //     case "cone":
            //         newGeometryEl.setAttribute('geometry', "primitive: cone; height: 0.4; radius-bottom: 0.2; radius-top: 0");
            //         break;
            //     case "cylinder":
            //         newGeometryEl.setAttribute('geometry', "primitive: cylinder; radius: 0.2; height: 0.4");
            //         break;
            //     case "dodecahedron":
            //         newGeometryEl.setAttribute('geometry', "primitive: dodecahedron; radius: 0.2");
            //         break;
            //     case "octahedron":
            //         newGeometryEl.setAttribute('geometry', "primitive: octahedron; radius: 0.2");
            //         break;
            //     case "sphere":
            //         newGeometryEl.setAttribute('geometry', "primitive: sphere; radius: 0.2");
            //         break;
            //     case "tetrahedron":
            //         newGeometryEl.setAttribute('geometry', "primitive: tetrahedron; radius: 0.2");
            //         break;
            //     case "torus":
            //         newGeometryEl.setAttribute('geometry', "primitive: torus; radius: 0.12; radiusTubular: 0.04");
            //         break;
            //     case "torusKnot":
            //         newGeometryEl.setAttribute('geometry', "primitive: torusKnot; radius: 0.12; radiusTubular: 0.02");
            //         break;
            // }
            // newGeometryEl.setAttribute('rotation', "90 0 0");
            // newGeometryEl.setAttribute('material', "shader: standard; color: #999999");
            // newGeometryEl.setAttribute('sound', "on: click; src: #click-sound");
            // newGeometryEl.setAttribute('stackable', "on: click");
            // newWrapperEl.appendChild(newGeometryEl);
            // sceneEl.appendChild(newWrapperEl);


            var clone = entityEl.cloneNode(true);
            sceneEl.appendChild(clone);

            var calculated_position = new THREE.Vector3().addVectors(markerEl.object3D.position, direction.clone().normalize().multiplyScalar(scale));
            entityEl.setAttribute('position', calculated_position);
            entityEl.setAttribute('visible', true);


            var lookAtTarget = new THREE.Vector3().addVectors(calculated_position, direction);
            entityEl.object3D.lookAt(lookAtTarget);
            entityEl.removeAttribute('id');

            // var entityEl = document.querySelector('#prototype');
            //
            // var calculated_position = new THREE.Vector3().addVectors(markerEl.object3D.position, direction.clone().normalize().multiplyScalar(scale));
            // entityEl.setAttribute('position', calculated_position);
            //
            //
            // var lookAtTarget = new THREE.Vector3().addVectors(calculated_position, direction);
            // // entityEl.object3D.lookAt(lookAtTarget);
        });
    }
});

AFRAME.registerComponent('pressable', {
    schema: {
        on: {type: 'string'},
        src: {type: 'string'}
    },

    init: function () {
        var data = this.data;
        var el = this.el;
        var sceneEl = document.querySelector('a-scene');

        el.addEventListener(data.on, function (evt) {
            var axisEl = document.querySelector('#axis-system');
            var markerEl = document.querySelector('#marker');
            var markedEl = document.querySelector('#marked');
            var menuEl = document.querySelector('#menu');
            var avatarEl = document.querySelector('#avatar');
            var location = markerEl.getAttribute('position');
            if (markedEl != null) {
                var scale = markedEl.getAttribute('scale');
                var rotation = markedEl.getAttribute('rotation');
                var position = markedEl.getAttribute('position');
            }
            var menu_main = document.querySelector('#menu-main');
            var menu_geometry = document.querySelector('#menu-geometry');
            var menu_property = document.querySelector('#menu-property');
            var menu_resize = document.querySelector('#menu-resize');
            var menu_rotate = document.querySelector('#menu-rotate');
            var menu_reposition = document.querySelector('#menu-reposition');
            var wormhole = document.querySelector('#wormhole');
            switch (data.src) {
                case "tp":
                    wormhole.setAttribute('visible', "true");
                    wormhole.emit('teleportation-start');
                    setTimeout(function () {
                        avatarEl.setAttribute('position', location.x + " 1 " + location.z);
                        markerEl.setAttribute('visible', "false");
                        menuEl.setAttribute('visible', "false");
                        wormhole.setAttribute('visible', "false");
                    }, 500);
                    break;
                case "go_to_geometry":
                    menu_main.setAttribute('position', "0 0 -1");
                    menu_main.setAttribute('visible', "false");
                    menu_geometry.setAttribute('position', "0 0 0");
                    menu_geometry.setAttribute('visible', "true");
                    break;
                case "go_to_property":
                    if (markedEl != null) {
                        menu_main.setAttribute('position', "0 0 -1");
                        menu_main.setAttribute('visible', "false");
                        menu_property.setAttribute('position', "0 0 0");
                        menu_property.setAttribute('visible', "true");
                    }
                    break;
                case "back_to_main":
                    menu_geometry.setAttribute('position', "0 0 -1");
                    menu_geometry.setAttribute('visible', "false");
                    menu_property.setAttribute('position', "0 0 -1");
                    menu_property.setAttribute('visible', "false");
                    menu_main.setAttribute('position', "0 0 0");
                    menu_main.setAttribute('visible', "true");
                    break;
                case "go_to_resize":
                    menu_property.setAttribute('position', "0 0 -1");
                    menu_property.setAttribute('visible', "false");
                    menu_resize.setAttribute('position', "0 0 0");
                    menu_resize.setAttribute('visible', "true");
                    break;
                case "go_to_rotate":
                    menu_property.setAttribute('position', "0 0 -1");
                    menu_property.setAttribute('visible', "false");
                    menu_rotate.setAttribute('position', "0 0 0");
                    menu_rotate.setAttribute('visible', "true");
                    break;
                case "go_to_reposition":
                    menu_property.setAttribute('position', "0 0 -1");
                    menu_property.setAttribute('visible', "false");
                    menu_reposition.setAttribute('position', "0 0 0");
                    menu_reposition.setAttribute('visible', "true");
                    break;
                case "back_to_property":
                    menu_rotate.setAttribute('position', "0 0 -1");
                    menu_rotate.setAttribute('visible', "false");
                    menu_resize.setAttribute('position', "0 0 -1");
                    menu_resize.setAttribute('visible', "false");
                    menu_reposition.setAttribute('position', "0 0 -1");
                    menu_reposition.setAttribute('visible', "false");
                    menu_property.setAttribute('position', "0 0 0");
                    menu_property.setAttribute('visible', "true");
                    break;
                case "del":
                    var copy = axisEl.cloneNode(true);
                    sceneEl.appendChild(copy);
                    axisEl.parentNode.removeChild(axisEl);
                    markedEl.parentNode.parentNode.removeChild(markedEl.parentNode);
                    markerEl.setAttribute('visible', false);
                    menuEl.setAttribute('visible', false);
                    break;
                case "scale_down":
                    markedEl.setAttribute('scale', new THREE.Vector3(scale.x * 0.9, scale.y * 0.9, scale.z * 0.9));
                    break;
                case "scale_up":
                    markedEl.setAttribute('scale', new THREE.Vector3(scale.x / 0.9, scale.y / 0.9, scale.z / 0.9));
                    break;
                case "rotate_x_c":
                    markedEl.setAttribute('rotation', new THREE.Vector3(rotation.x - 15, rotation.y, rotation.z));
                    break;
                case "rotate_x_cc":
                    markedEl.setAttribute('rotation', new THREE.Vector3(rotation.x + 15, rotation.y, rotation.z));
                    break;
                case "rotate_y_c":
                    markedEl.setAttribute('rotation', new THREE.Vector3(rotation.x, rotation.y - 15, rotation.z));
                    break;
                case "rotate_y_cc":
                    markedEl.setAttribute('rotation', new THREE.Vector3(rotation.x, rotation.y + 15, rotation.z));
                    break;
                case "rotate_z_c":
                    markedEl.setAttribute('rotation', new THREE.Vector3(rotation.x, rotation.y, rotation.z - 15));
                    break;
                case "rotate_z_cc":
                    markedEl.setAttribute('rotation', new THREE.Vector3(rotation.x, rotation.y, rotation.z + 15));
                    break;
                case "move_x_forward":
                    markedEl.setAttribute('position', new THREE.Vector3(position.x - 0.05, position.y, position.z));
                    break;
                case "move_x_backward":
                    markedEl.setAttribute('position', new THREE.Vector3(position.x + 0.05, position.y, position.z));
                    break;
                case "move_y_forward":
                    markedEl.setAttribute('position', new THREE.Vector3(position.x, position.y - 0.05, position.z));
                    break;
                case "move_y_backward":
                    markedEl.setAttribute('position', new THREE.Vector3(position.x, position.y + 0.05, position.z));
                    break;
                case "move_z_forward":
                    markedEl.setAttribute('position', new THREE.Vector3(position.x, position.y, position.z - 0.05));
                    break;
                case "move_z_backward":
                    markedEl.setAttribute('position', new THREE.Vector3(position.x, position.y, position.z + 0.05));
                    break;
            }

        });
    }
});

function registerComponent(name, definition) {
    console.log("trying to port");
    AFRAME.registerComponent(name, definition);
}