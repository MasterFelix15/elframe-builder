module CustomComponent exposing (..)

import Html.Attributes exposing (attribute)
import Html exposing (Attribute)


pressable : String -> Attribute msg
pressable value = attribute "pressable" value


stackable : String -> Attribute msg
stackable value = attribute "stackable" value


selectable : String -> Attribute msg
selectable value = attribute "selectable" value


lookAt : String -> Attribute msg
lookAt value = attribute "look-at" value