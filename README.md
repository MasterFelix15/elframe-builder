# Elframe -- A-Frame with ELM

## Getting started
To install all dependencies and start server:

    cd elframe
    npm install
    npm run start
    
Then you can connect to [http://localhost:8080](http://localhost:8080). 

## Connecting from another device
If you want to connect from another device, for example your mobile phone, you need to enable remote connections:

* Open `package.json`
* Look for the line:  
    `"start": "webpack-dev-server --hot --inline --content-base src/",`
* Replace it with:  
    `"start": "webpack-dev-server --hot --host 0.0.0.0 --inline --content-base src/",`
    
